
// Models and enums
import { Container } from "@common/models/container";
import { STATUSES } from "./statuses.enum";

// Services
import { WorkspaceService } from "@common/services/workspace.service";

export class Workspace {
    container!: Container;
    contents?: Container[];
 
    constructor(
        private workspaceService: WorkspaceService
    ) { }

}
