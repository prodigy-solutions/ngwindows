// Models and enums
import { STATUSES } from "@common/models/statuses.enum";

// Services
import { ContainerService } from "@common/services/container.service";

export class Container {
    id?: string;
    name!: string;

    status!: STATUSES;
    
    draggable!: boolean;
    resizable!: boolean;
    expandableX!: boolean;
    expandableY!: boolean;

    zIndex!: number;

    stacked!: boolean;
    expanded!: boolean;

    parent!: Container;
    contents?: Container[];

    x!: number;
    y!: number;

    minX?: number;
    maxX?: number;
    minY?: number;
    maxY?: number;

    snapX?: number;
    snapY?: number;

    width!: number;
    height!: number;
    minWidth?: number;
    maxWidth?: number;
    minHeight?: number;
    maxHeight?: number;
    title?: string;
    icon?: string


    constructor(
        private containerService: ContainerService
    ) {}
}
