export enum STATUSES {
    "new",
    "active",
    "selected",
    "blocked",
    "dragging",
    "hidden"
}