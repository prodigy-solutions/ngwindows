import { Component, OnInit, TemplateRef, ViewChild, ViewContainerRef } from '@angular/core';
import { CommonModule } from '@angular/common';

import { Container } from '@common/models/container';
import { ContainerService } from '@common/services/container.service';

@Component({
  selector: 'app-container',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './container.component.html',
  styleUrls: ['./container.component.scss']
})
export class ContainerComponent implements OnInit  {

  @ViewChild('template', { read: TemplateRef }) template!: TemplateRef<any>;
  @ViewChild('viewContainer', {read: ViewContainerRef}) viewContainer!: ViewContainerRef;

  public container!: Container;

  public title!: string;

  constructor(
    private containerService: ContainerService
  ) { }

  ngOnInit() {
    this.container = new Container(this.containerService);
  }

}
