import { Component, OnInit, ViewChild, ViewContainerRef, TemplateRef } from '@angular/core';
import { CommonModule } from '@angular/common';


// Components
import { FooterComponent } from '@common/components/footer/footer.component';
import { HeaderComponent } from '@common/components/header/header.component';
import { ContainerComponent } from '../container/container.component';

// Services
import { WorkspaceService } from '@common/services/workspace.service';

// Models
import { ScreenDimensions } from '@common/models/screen-dimensions';

@Component({
  selector: 'app-workspace',
  standalone: true,
  imports: [CommonModule, FooterComponent, HeaderComponent, ContainerComponent],
  templateUrl: './workspace.component.html',
  styleUrls: ['./workspace.component.scss']
})
export class WorkspaceComponent implements OnInit {

  @ViewChild('workspaceElement') workspaceElement!: HTMLElement;
  @ViewChild('template', { read: TemplateRef }) template!: TemplateRef<any>;
  @ViewChild('viewContainer', {read: ViewContainerRef}) viewContainer!: ViewContainerRef;

  public contentHeight!: number;
  public screenDimensions!: ScreenDimensions;

  public contentOffsetY = 182;

  constructor(
    private workspaceService: WorkspaceService,
  ) { }

  ngOnInit(): void {
    // Init content size
    this.onWindowResize();
    window.addEventListener('resize', $resizeEvent => this.onWindowResize());
  }

  onWindowResize():void {
    this.screenDimensions = this.workspaceService.getScreenDimensions();
    this.contentHeight = this.screenDimensions.height - this.contentOffsetY;
  }

}
