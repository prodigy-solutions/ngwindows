import { Component, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'app-stack-container',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './stack-container.component.html',
  styleUrls: ['./stack-container.component.scss']
})
export class StackContainerComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
