import { ComponentFixture, TestBed } from '@angular/core/testing';

import { StackContainerComponent } from './stack-container.component';

describe('StackContainerComponent', () => {
  let component: StackContainerComponent;
  let fixture: ComponentFixture<StackContainerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [ StackContainerComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(StackContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
