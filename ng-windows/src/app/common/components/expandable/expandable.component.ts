import { Component, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'app-expandable',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './expandable.component.html',
  styleUrls: ['./expandable.component.scss']
})
export class ExpandableComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
