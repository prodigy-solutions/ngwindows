import { Component, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'app-tristate-button',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './tristate-button.component.html',
  styleUrls: ['./tristate-button.component.scss']
})
export class TristateButtonComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
