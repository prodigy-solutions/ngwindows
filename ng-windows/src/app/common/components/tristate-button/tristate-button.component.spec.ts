import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TristateButtonComponent } from './tristate-button.component';

describe('TristateButtonComponent', () => {
  let component: TristateButtonComponent;
  let fixture: ComponentFixture<TristateButtonComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [ TristateButtonComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(TristateButtonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
