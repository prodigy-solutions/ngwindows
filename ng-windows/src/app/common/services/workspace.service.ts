import { Injectable } from '@angular/core';

// Models
import { ScreenDimensions } from '@common/models/screen-dimensions';
@Injectable({
  providedIn: 'root'
})

export class WorkspaceService {

  screenDimensions!: ScreenDimensions;


  constructor( ) { }

  setScreenDimensions(screenDimensions: ScreenDimensions): void {
    this.screenDimensions = screenDimensions;
  }

  getScreenDimensions(): ScreenDimensions {
    return this.screenDimensions;
  }

  addDynamicComponent(component: any, target: any):void {
    //target.appendChild(component);
  }

}
