import { Component, OnInit } from '@angular/core';

// Models
import { ScreenDimensions } from '@common/models/screen-dimensions';

// Services
import { WorkspaceService } from "@common/services/workspace.service";
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'ng-windows';

  public screenDimensions = new ScreenDimensions();

  constructor(
    private workspaceService: WorkspaceService
  ) {}

  ngOnInit(): void {
    this.onWindowResize();

    window.addEventListener('resize', $resizeEvent => this.onWindowResize());
  }

  onWindowResize():void {
    this.screenDimensions.width = window.innerWidth;
    this.screenDimensions.height = window.innerHeight;
    this.workspaceService.setScreenDimensions(this.screenDimensions);
  }

}
