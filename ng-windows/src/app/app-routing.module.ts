import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

// Components
import { WorkspaceComponent } from '@common/components/workspace/workspace.component';

const routes: Routes = [

  { path: '', redirectTo: '/workspace', pathMatch: 'full' },
  {
    path: "workspace",
    component: WorkspaceComponent
  },

  // {
  //   path: "error",
  //   component: ErrorPageComponent
  // },

  //Wild Card Route for 404 request
  // {
  //   path: '**',
  //   pathMatch: 'full', 
  //   component: NotFoundPageComponent
  // },


];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
